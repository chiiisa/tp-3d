﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 20f;
    public Rigidbody rb;

    public int bounce = 2;

    void Start()
    {
        rb.velocity = transform.forward * speed;

    }

    private void OnCollisionEnter(Collision collision)
    {
        var hit = collision.gameObject;
        if(hit.tag == "Enemy")
        {
            Destroy(hit.gameObject);
            Destroy(gameObject);
        }
        else if (hit.tag == "Shoot" )
        {
            Destroy(gameObject);
        }
        else if(hit)
        {
            bounce--;
            if (bounce <= 0)
            {
                Destroy(gameObject);
            }
            Bounce(this.transform.position , this.transform.forward);
        }
    }

    void Bounce(Vector3 pos, Vector3 dir)
    {
        Ray ray = new Ray(pos, dir);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            dir = Vector3.Reflect(dir, hit.normal);
            rb.velocity = dir * speed;
            transform.rotation = Quaternion.FromToRotation(Vector3.forward, dir);
            Debug.Log("dir : " + dir);
            Debug.Log("rb.velocity" + rb.velocity);
            Debug.Log("hit.point" + hit.point);
            Debug.Log("hit normal" + hit.normal);

        }

    }
}
