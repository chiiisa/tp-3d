﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public Transform turretTranform;
    public Transform shootPoint;
    public Camera camera;
    public GameObject bulletPrefab;

    private float inputH;
    private float inputV;
    private float tankSpeed = 3f;
    private int EnemyCount;

    private Vector3 reticlePosition;
    private Rigidbody rb;
    private GameManager GameManager;
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        GameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    private void Update()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }

        EnemyCount = GameObject.FindGameObjectsWithTag("Enemy").Length;
        if (EnemyCount <= 0)
        {
            StartCoroutine(LoadScene());
        }
    }

    void FixedUpdate()
    {
        inputV = Input.GetAxis("Vertical");
        inputH = Input.GetAxis("Horizontal");

        Vector3 pos = transform.position + (transform.forward * inputV * tankSpeed * Time.fixedDeltaTime);
        rb.MovePosition(pos);

        Quaternion Rotation = transform.rotation * Quaternion.Euler(Vector3.up * (50 * inputH * Time.fixedDeltaTime));
        rb.MoveRotation(Rotation);

        Ray screenRay = camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(screenRay, out hit))
        {
            reticlePosition = hit.point;
        }
        Vector3 turretLookDir = reticlePosition - turretTranform.position;
        turretLookDir.y = 0f;
        turretTranform.rotation = Quaternion.LookRotation(turretLookDir);
    }

    void Shoot()
    {
        Instantiate(bulletPrefab, new Vector3(shootPoint.position.x, 0.5f, shootPoint.position.z), turretTranform.rotation);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Shoot" && GameManager.PlayerLive >= 1)
        {
            GameManager.PlayerLive -= 1;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            Destroy(gameObject);
        }
        if (collision.collider.tag == "Shoot" && GameManager.PlayerLive <= 0)
        {
            SceneManager.LoadScene(5);
        }

    }

    IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
