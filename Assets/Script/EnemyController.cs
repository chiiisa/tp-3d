﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class EnemyController : MonoBehaviour
{
    public ParticleSystem deadParticle;
    public GameObject bulletPrefab;
    public Transform turretTranform;
    public Transform shootPoint;

    private float MaxDistance = 15;
    private bool CanShoot;

    void Start()
    {
        CanShoot = true;
    }


    void Update()
    {
        float angle = Mathf.Sin(Time.time) * -50;
        turretTranform.rotation = Quaternion.AngleAxis(-80 + angle, Vector3.up);

        EnemyShoot(turretTranform.position + turretTranform.forward * 0.75f, turretTranform.forward, 2);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Shoot" || collision.collider.tag == "Player")
        {
            Instantiate(deadParticle, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }

    private void EnemyShoot(Vector3 position, Vector3 direction, int number)
    {
        if (number == 0)
            return;

        Ray ray = new Ray(position, direction);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, MaxDistance))
        {
            direction = Vector3.Reflect(direction, hit.normal);
            position = hit.point;
            if (hit.collider.tag == "Player" && CanShoot == true)
            {
                Instantiate(bulletPrefab, new Vector3(shootPoint.position.x, 0.5f, shootPoint.position.z), turretTranform.rotation);
                StartCoroutine(Shoot());
                float angle = Mathf.Sin(Time.time) * -50;
                turretTranform.rotation = Quaternion.AngleAxis(-80 + angle, Vector3.up);
            }

        }
        EnemyShoot(position, direction, number - 1);
    }

    IEnumerator Shoot()
    {
        CanShoot = false;
        yield return new WaitForSeconds(1f);
        CanShoot = true;
    }

   

}
