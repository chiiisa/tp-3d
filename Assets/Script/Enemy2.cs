﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Enemy2 : MonoBehaviour
{
    private float rayDistance = 5.0f;
    private float stoppingDistance = 1.5f;
    private bool CanShoot;

    private Vector3 destination;
    private Quaternion desiredRotation;
    private Vector3 direction;
    private PlayerController target;

    Quaternion StartingAngle = Quaternion.AngleAxis(-25, Vector3.up);
    Quaternion stepAngle = Quaternion.AngleAxis(5, Vector3.up);

    [SerializeField] State currentState;
    [SerializeField] Transform shoot;
    [SerializeField] GameObject ShootPrefab;
    [SerializeField] ParticleSystem deadParticle;

    public enum State
    {
        walk,
        Attack
    }
    private void Start()
    {
        CanShoot = true;
    }

    void Update()
    {
        switch (currentState)
        {
            case State.walk:
                {
                    if (NeedsDestination())
                    {
                        GetDestination();
                    }

                    transform.rotation = desiredRotation;

                    transform.Translate(Vector3.forward * Time.deltaTime * 5f);

                    while (IsPathBlocked()) 
                    {
                        GetDestination();
                    }

                    Transform targetToAggro = CheckForArro();
                    if (targetToAggro != null)
                    {
                        target = targetToAggro.GetComponent<PlayerController>();
                        transform.LookAt(target.transform);
                        currentState = State.Attack;
                    }
                    break;

                }
            case State.Attack:
                {
                    if (CanShoot)
                    { 
                        Instantiate(ShootPrefab, shoot.position, shoot.rotation);
                        StartCoroutine(Shoot());
                    }
                    break;
                }
        }
    }

    private bool NeedsDestination()
    {
        if (destination == Vector3.zero)
            return true;

        float distance = Vector3.Distance(transform.position, destination);
        if (distance <= stoppingDistance)
            return true;

        return false;
    }

    private void GetDestination()
    {
        Vector3 testPosition = (transform.position + (transform.forward * 4f)) + new Vector3(Random.Range(-4.5f, 4.5f), 0f, Random.Range(-4.5f, 4.5f));

        destination = new Vector3(testPosition.x, 1f, testPosition.z);

        direction = Vector3.Normalize(destination - transform.position);
        direction = new Vector3(direction.x, 0f, direction.z);
        desiredRotation = Quaternion.LookRotation(direction);

    }

    private bool IsPathBlocked()
    {
        Ray ray = new Ray(transform.position, direction);
        var hitSomething = Physics.RaycastAll(ray, rayDistance);
        return hitSomething.Any();
    }

    private Transform CheckForArro()
    {
        float radius = 10f;

        RaycastHit hit;
        Quaternion angle = transform.rotation * StartingAngle;
        Vector3 direction = angle * Vector3.forward;
        Vector3 pos = transform.position;
        for(var i = 0; i< 10; i++)
        {
            if (Physics.Raycast(pos, direction, out hit, radius))
            {
                var player = hit.collider.GetComponent<PlayerController>();
                if (player != null)
                {
                    Debug.DrawRay(pos, direction * hit.distance, Color.red);
                    return player.transform;
                }
            }
            direction = stepAngle * direction;
        }
        return null;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Shoot" || collision.collider.tag == "Player")
        {
            Instantiate(deadParticle, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }

    IEnumerator Shoot()
    {
        CanShoot = false;
        yield return new WaitForSeconds(0.5f);
        currentState = State.walk;
        CanShoot = true;
    }

}
