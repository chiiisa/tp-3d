﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIscript : MonoBehaviour
{
    //public static UIscript uIscript { get; private set; }
    public GameObject infoNiveau;
    //public Text EnemyText;
    //public Text level;
    //public Text PlayerLiveText;

    //private int EnemyCount;
    //public int PlayerLive;

    /*void Awake()
    {
       if(uIscript == null)
       {
            uIscript = this;
            DontDestroyOnLoad(gameObject);
       }
        else
        {
            Destroy(gameObject);
        }
    }*/
    void Start()
    {
        StartCoroutine(TimeCountdown());
    }
    private void Update()
    {
        /*PlayerLiveText.text = "Live" + PlayerLive;
        EnemyCount = GameObject.FindGameObjectsWithTag("Enemy").Length;
        Debug.Log(EnemyCount);
        if (EnemyCount <=0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex +1);
            StartCoroutine(TimeCountdown());
        }*/
    }

    IEnumerator TimeCountdown()
    {
        infoNiveau.SetActive(true);
        /*EnemyText.text = "Enemy Tanks " + EnemyCount;
        Debug.Log("EnemyCount : " + EnemyCount);
        level.text = "Level " + SceneManager.GetActiveScene().buildIndex;*/
        Time.timeScale = 0;
        float pauseTime = Time.realtimeSinceStartup + 3f;
        while (Time.realtimeSinceStartup < pauseTime)
            yield return 0;
        infoNiveau.SetActive(false);
        Time.timeScale = 1;
    }
}
